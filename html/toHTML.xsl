<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output method="html" indent="yes" />
    <xsl:variable name="styleSheetPath">../html/style.css</xsl:variable>
    <xsl:variable name="index">index.html</xsl:variable>

    <xsl:template match="all">
        <html>
            <head>
                <meta charset="utf-8"/>
                <link rel="stylesheet" href="{$styleSheetPath}"/>
                <title>Homepage of The World Factbook</title>
            </head>
            <body>
                <header>
                    <span>BI-XML</span>
                </header>
                <main>
                    <div class="box">
                        <h1>The WorldFactbook</h1>
                        <div class="nav">
                            <div class="index">
                                <ul>
                                    <xsl:for-each select="./country">
                                        <li>
                                            <xsl:variable name="link">
                                                <xsl:value-of select="translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ ','abcdefghijklmnopqrstuvwxyz_')"/>
                                            </xsl:variable>
                                            <a href="{$link}.html">
                                                <xsl:value-of select="@name"/>
                                            </a>
                                        </li>
                                    </xsl:for-each>
                                </ul>
                            </div>
                        </div>
                        <div class="info">
                            <h2>Semestral Work</h2>
                            <div class="info-content">
                                <p>This is semestral work from BI-XML on CTU FIT.</p>
                                <p>The author has chosen 4 countries, extracted data about the countries from <a href="">The World Factbook</a> and created 4 XML files with using XSLT stylesheet.</p>
                                <p>Check out the retrieved information!</p>
                            </div>
                        </div>
                    </div>
                </main>
            </body>
        </html>
        <xsl:apply-templates select="country"/>
    </xsl:template>

    <xsl:template match="country">
        <xsl:variable name="countryName"><xsl:value-of select="@name"/></xsl:variable>
        <xsl:variable name="fileName">
            <xsl:value-of select="translate($countryName,' ABCDEFGHIJKLMNOPQRSTUVWXYZ ','_abcdefghijklmnopqrstuvwxyz')"/>
        </xsl:variable>
        <xsl:result-document href="{$fileName}.html" method="html">
            <html>
                <head>
                    <meta charset="utf-8"/>
                    <link rel="stylesheet" href="{$styleSheetPath}"/>
                    <title><xsl:value-of select="@name"/></title>
                </head>
                <body>
                    <header>
                        <a href="{$index}">BACK TO ALL COUNTRIES</a>
                    </header>
                    <main>
                        <div class="nav">
                            <p>Contents</p>
                            <ul>
                                <li>
                                    <a href="#Gallery">Gallery</a>
                                </li>
                                <xsl:for-each select="./categories/*">
                                    <li>
                                        <a href="#{@name}">
                                            <xsl:value-of select="@name"/>
                                        </a>
                                    </li>
                                </xsl:for-each>
                            </ul>
                        </div>
                        <div class="content">
                            <h1>
                                <xsl:value-of select="@name"/>
                            </h1>
                            <div id="Gallery" class="images">
                                <h2>Gallery</h2>
                                <xsl:for-each select="images/image">
                                    <xsl:copy-of select="."/>
                                </xsl:for-each>
                            </div>
                                <xsl:apply-templates/>
                        </div>
                    </main>
                    <footer>
                        <p>ČVUT - FIT</p>
                        <p>BI-XML</p>
                        <p>2022</p>
                        <p>Made by: Šimon Melkes</p>
                        <p>melkesim@cvut.cz</p>
                    </footer>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>


    <xsl:template match="categories">
        <ul id="categories">
            <xsl:for-each select="child::*">
                <li id="{@name}">
                    <xsl:apply-templates select="."/>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>

    <xsl:template match="category">
        <h2><xsl:value-of select="@name"/></h2>
        <div class="category">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="section">
        <h3><xsl:value-of select="@name"/></h3>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="text[not(@name)]">
        <div class="text-content">
            <p><xsl:value-of select="."/></p>
        </div>
    </xsl:template>

    <xsl:template match="text[@name]">
        <div class="text-content">
            <div>
                <span class="h4">
                    <xsl:value-of select="@name"/>:
                </span>
                <span>
                    <xsl:value-of select="."/>
                </span>
            </div>
        </div>
    </xsl:template>

</xsl:stylesheet>
