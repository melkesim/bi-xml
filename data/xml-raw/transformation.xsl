<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:output method="xml" indent="yes" />
<xsl:strip-space elements="*"/>

    <xsl:template match="/*">
        <country>
            <categories>
                <xsl:for-each select="child::*">
                    <xsl:call-template name="category"/>
                </xsl:for-each>
            </categories>
        </country>
    </xsl:template>

    <xsl:template match="//*[text]" name="textTemplate" priority="2">
        <text name="{replace(name(),'(_)',' ')}">
            <xsl:value-of select="." />
        </text>
    </xsl:template>

    <xsl:template match="/*/*/*[text]" priority="3">
        <xsl:call-template name="section"/>
    </xsl:template>

    <xsl:template match="//text">
        <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="//note">
        <xsl:call-template name="textTemplate"/>
    </xsl:template>

    <xsl:template name="category">
        <category name="{replace(name(),'(_)',' ')}">
            <xsl:apply-templates/>
        </category>
    </xsl:template>

    <xsl:template match="/*/*/*" name="section">
        <section name="{replace(name(),'(_)',' ')}">
            <xsl:apply-templates/>
        </section>
    </xsl:template>

</xsl:stylesheet>

