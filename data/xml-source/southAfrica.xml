<?xml version="1.0" encoding="UTF-8"?>
<country name="South Africa">
   <images>
      <image src="https://www.cia.gov/the-world-factbook/static/9bf621dde7ff8640a93109c3ff38c7d3/d786d/SF_010_large.jpg" alt="The reddish-brown Kalahari Desert with its northwest-southeast-trending sand dunes and dry lakebeds stands out in this image of South Africa. Also distinguishable are the westward-flowing Orange River, south of the true desert; the cape ranges of folded mountains near the extreme southern point of South Africa; as well as Cape Town, the Cape of Good Hope, and Cape Agulhas. Image courtesy of NASA."/>
      <image src="https://www.cia.gov/the-world-factbook/static/9dea7fd20a25dfe7cda6360c919cd842/696bb/SF_009_large.jpg" alt="Satellite view of South Africa. The rugged Great Karoo semi-desert region makes up much of the central and western part of the country. The brown and orange landscape that surrounds South Africa&apos;s northwestern borders is the Kalahari Desert, a vast sand basin marked by dunes and dry savannah vegetation. The southern edge of the desert is defined by the Orange River, which also forms South Africa&apos;s northwestern border with Namibia. Within South Africa is the enclave of Lesotho. Northeast of Lesotho is the smaller country of Eswatini (formerly named Swaziland). Photo courtesy of NASA."/>
      <image src="https://www.cia.gov/the-world-factbook/static/906abcadcf3e301846b456bd014ee338/d786d/SF_011_large.jpg" alt="Cape Town is considered to be one of the great scenic cities of the world. Situated in the southwestern part of Cape Province, it lies at the foot of Table Mountain (1,088 m; 3,567 feet in altitude) on the shore of Table Bay, in which lies Robben Island. Because the mountains obstruct inland expansion, the city has developed along the coast. The Cape of Good Hope appears at the southern tip of the Cape Peninsula. Photo courtesy of NASA."/>
      <image src="https://www.cia.gov/the-world-factbook/static/20e1fcf0d50238a4d56eea5681cec72e/3cbc4/SF_008_large.jpg" alt="Shoreline along the Cape of Good Hope."/>
      <image src="https://www.cia.gov/the-world-factbook/static/82e5cb972ad87309e5ef096f31e73ffc/8a07b/SF_007_large.jpg" alt="Observation tower at the Cape of Good Hope."/>
      <image src="https://www.cia.gov/the-world-factbook/static/8560d2f8c4fc94eb3bd432c7abb946d2/3cbc4/SF_006_large.jpg" alt="The Cape of Good Hope."/>
      <image src="https://www.cia.gov/the-world-factbook/static/1a204a1a84b5de48dd33b27fb8e46592/3cbc4/SF_005_large.jpg" alt="Local residents at the Cape of Good Hope - Chacma or Cape Baboons."/>
      <image src="https://www.cia.gov/the-world-factbook/static/eb52097b2939339412b17cc2d62d8ba4/3cbc4/SF_003_large.jpg" alt="Sculpture at a Cape Town shopping mall."/>
      <image src="https://www.cia.gov/the-world-factbook/static/fe58854ca35205731f1812cc7d8f85bb/3cbc4/SF_004_large.jpg" alt="Table Mountain - a level plataeu about 3 km (2 mi) from side to side - overlooking some homes in Cape Town."/>
      <image src="https://www.cia.gov/the-world-factbook/static/e80306db34ff934662d9934df721ecce/21dc3/SF_002_large.jpg" alt="The picturesque Victoria and Alfred Waterfront in Cape Town, affords photographers a wide range of opportunities. With Table Mountain as a scenic backdrop, marine vessels of all kinds can be seen coming and going from this working harbor and residential marina."/>
      <image src="https://www.cia.gov/the-world-factbook/static/80fd0380c086c21712ae36ac667b6c5a/712b0/IMAG0116.jpg" alt="Cape Town waterfront. Pleasure boats, ferries, and cranes fill the harbor."/>
      <image src="https://www.cia.gov/the-world-factbook/static/b0a30700b59ed56ac517ce127e666371/712b0/IMAG0120.jpg" alt="Mountains seen from the vantage of Cape Town Harbor."/>
   </images>
   <categories>
      <category name="Introduction">
         <section name="Background">
            <text>Some of the earliest human remains in the fossil record are found in South Africa. By about A.D. 500, Bantu speaking groups began settling into what is now northeastern South Africa displacing Khoisan speaking groups to the southwest. Dutch traders landed at the southern tip of present-day South Africa in 1652 and established a stopover point on the spice route between the Netherlands and the Far East, founding the city of Cape Town. After the British seized the Cape of Good Hope area in 1806, many of the settlers of Dutch descent (Afrikaners, also called "Boers" (farmers) at the time) trekked north to found their own republics, Transvaal and Orange Free State. In the 1820s, several decades of wars began as the Zulus expanded their territory, moving out of what is today southeastern South Africa and clashing with other indigenous peoples and with expanding European settlements. The discovery of diamonds (1867) and gold (1886) spurred wealth and immigration from Europe.The Anglo-Zulu War (1879) resulted in the incorporation of the Zulu kingdom's territory into the British Empire. Subsequently, the Afrikaner republics were incorporated into the British Empire after their defeat in the Second South African War (1899-1902). However, the British and the Afrikaners ruled together beginning in 1910 under the Union of South Africa, which became a republic in 1961 after a whites-only referendum. In 1948, the National Party was voted into power and instituted a policy of apartheid – billed as "separate development" of the races - which favored the white minority at the expense of the black majority and other non-white groups. The African National Congress (ANC) led the opposition to apartheid and many top ANC leaders, such as Nelson MANDELA, spent decades in South Africa's prisons. Internal protests and insurgency, as well as boycotts by some Western nations and institutions, led to the regime's eventual willingness to negotiate a peaceful transition to majority rule. The first multi-racial elections in 1994 following the end of apartheid ushered in majority rule under an ANC-led government. South Africa has since struggled to address apartheid-era imbalances in wealth, housing, education, and health care. Jacob ZUMA became president in 2009 and was reelected in 2014, but resigned in February 2018 after numerous corruption scandals and gains by opposition parties in municipal elections in 2016. His successor, Cyril RAMAPHOSA, has made some progress in reigning in corruption, though many challenges persist. In May 2019 national elections, the country’s sixth since the end of apartheid, the ANC won a majority of parliamentary seats, delivering RAMAPHOSA a five-year term.</text>
         </section>
      </category>
      <category name="Geography">
         <section name="Location">
            <text>Southern Africa, at the southern tip of the continent of Africa</text>
         </section>
         <section name="Geographic coordinates">
            <text>29 00 S, 24 00 E</text>
         </section>
         <section name="Map references">
            <text>Africa</text>
         </section>
         <section name="Area">
            <text name="total">1,219,090 sq km</text>
            <text name="land">1,214,470 sq km</text>
            <text name="water">4,620 sq km</text>
            <text name="note">includes Prince Edward Islands (Marion Island and Prince Edward Island)</text>
         </section>
         <section name="Area - comparative">
            <text>slightly less than twice the size of Texas</text>
         </section>
         <section name="Land boundaries">
            <text name="total">5,244 km</text>
            <text name="border countries">Botswana 1,969 km; Lesotho 1,106 km; Mozambique 496 km; Namibia 1,005 km; Eswatini 438 km; Zimbabwe 230 km</text>
         </section>
         <section name="Coastline">
            <text>2,798 km</text>
         </section>
         <section name="Maritime claims">
            <text name="territorial sea">12 nm</text>
            <text name="contiguous zone">24 nm</text>
            <text name="exclusive economic zone">200 nm</text>
            <text name="continental shelf">200 nm or to edge of the continental margin</text>
         </section>
         <section name="Climate">
            <text>mostly semiarid; subtropical along east coast; sunny days, cool nights</text>
         </section>
         <section name="Terrain">
            <text>vast interior plateau rimmed by rugged hills and narrow coastal plain</text>
         </section>
         <section name="Elevation">
            <text name="highest point">Ntheledi (Mafadi) 3,450 m</text>
            <text name="lowest point">Atlantic Ocean 0 m</text>
            <text name="mean elevation">1,034 m</text>
         </section>
         <section name="Natural resources">
            <text>gold, chromium, antimony, coal, iron ore, manganese, nickel, phosphates, tin, rare earth elements, uranium, gem diamonds, platinum, copper, vanadium, salt, natural gas</text>
         </section>
         <section name="Irrigated land">
            <text>16,700 sq km (2012)</text>
         </section>
         <section name="Major rivers  by length in km ">
            <text>Orange (shared with Lesotho [s], and Namibia [m]) - 2,092 km; Limpopo river source (shared with Botswana, Zimbabwe, and Mozambique [m]) - 1,800 km; Vaal [s] - 1,210 kmnote – [s] after country name indicates river source; [m] after country name indicates river mouth</text>
         </section>
         <section name="Major watersheds  area sq km ">
            <text>Atlantic Ocean drainage: Orange (941,351 sq km)</text>
         </section>
         <section name="Major aquifers">
            <text>Karoo Basin, Lower Kalahari-Stampriet Basin</text>
         </section>
         <section name="Population distribution">
            <text>the population concentrated along the southern and southeastern coast, and inland around Pretoria; the eastern half of the country is more densly populated than the west as shown in this population distribution map</text>
         </section>
         <section name="Natural hazards">
            <text>prolonged droughtsvolcanism: the volcano forming Marion Island in the Prince Edward Islands, which last erupted in 2004, is South Africa's only active volcano</text>
         </section>
         <section name="Geography - note">
            <text>South Africa completely surrounds Lesotho and almost completely surrounds Eswatini</text>
         </section>
      </category>
      <category name="Environment">
         <section name="Environment - current issues">
            <text>lack of important arterial rivers or lakes requires extensive water conservation and control measures; growth in water usage outpacing supply; pollution of rivers from agricultural runoff and urban discharge; air pollution resulting in acid rain; deforestation; soil erosion; land degradation; desertification; solid waste pollution; disruption of fragile ecosystem has resulted in significant floral extinctions</text>
         </section>
         <section name="Environment - international agreements">
            <text name="party to">Antarctic-Environmental Protection, Antarctic-Marine Living Resources, Antarctic Seals, Antarctic Treaty, Biodiversity, Climate Change, Climate Change-Kyoto Protocol, Climate Change-Paris Agreement, Comprehensive Nuclear Test Ban, Desertification, Endangered Species, Hazardous Wastes, Law of the Sea, Marine Dumping-London Convention, Marine Dumping-London Protocol, Marine Life Conservation, Nuclear Test Ban, Ozone Layer Protection, Ship Pollution, Wetlands, Whaling</text>
            <text name="signed  but not ratified">none of the selected agreements</text>
         </section>
         <section name="Air pollutants">
            <text name="particulate matter emissions">23.58 micrograms per cubic meter (2016 est.)</text>
            <text name="carbon dioxide emissions">476.64 megatons (2016 est.)</text>
            <text name="methane emissions">55.89 megatons (2020 est.)</text>
         </section>
         <section name="Climate">
            <text>mostly semiarid; subtropical along east coast; sunny days, cool nights</text>
         </section>
         <section name="Urbanization">
            <text name="urban population">68.3% of total population (2022)</text>
            <text name="rate of urbanization">1.72% annual rate of change (2020-25 est.)</text>
         </section>
         <section name="Revenue from coal">
            <text name="coal revenues">2.4% of GDP (2018 est.)</text>
         </section>
         <section name="Major infectious diseases">
            <text name="degree of risk">intermediate (2020)</text>
            <text name="food or waterborne diseases">bacterial diarrhea, hepatitis A, and typhoid fever</text>
            <text name="water contact diseases">schistosomiasis</text>
            <text name="note">widespread ongoing transmission of a respiratory illness caused by the novel coronavirus (COVID-19) is occurring throughout South Africa; as of 6 October 2021, South Africa has reported a total of 2,907,619 cases of COVID-19 or 4,902.52 cumulative cases of COVID-19 per 100,000 population with 148.24 cumulative deaths per 100,000 population; as of 5 October 2021, 21.76% of the population has received at least one dose of COVID-19 vaccine</text>
         </section>
         <section name="Waste and recycling">
            <text name="municipal solid waste generated annually">18,457,232 tons (2011 est.)</text>
            <text name="municipal solid waste recycled annually">5,168,025 tons (2011 est.)</text>
            <text name="percent of municipal solid waste recycled">28% (2011 est.)</text>
         </section>
         <section name="Major rivers  by length in km ">
            <text>Orange (shared with Lesotho [s], and Namibia [m]) - 2,092 km; Limpopo river source (shared with Botswana, Zimbabwe, and Mozambique [m]) - 1,800 km; Vaal [s] - 1,210 kmnote – [s] after country name indicates river source; [m] after country name indicates river mouth</text>
         </section>
         <section name="Major watersheds  area sq km ">
            <text>Atlantic Ocean drainage: Orange (941,351 sq km)</text>
         </section>
         <section name="Major aquifers">
            <text>Karoo Basin, Lower Kalahari-Stampriet Basin</text>
         </section>
         <section name="Total water withdrawal">
            <text name="municipal">3.89 billion cubic meters (2017 est.)</text>
            <text name="industrial">4.1 billion cubic meters (2017 est.)</text>
            <text name="agricultural">11.39 billion cubic meters (2017 est.)</text>
         </section>
         <section name="Total renewable water resources">
            <text>51.35 billion cubic meters (2017 est.)</text>
         </section>
      </category>
      <category name="Energy">
         <section name="Electricity access">
            <text name="electrification - total population">94% (2019)</text>
            <text name="electrification - urban areas">95% (2019)</text>
            <text name="electrification - rural areas">92% (2019)</text>
         </section>
         <section name="Electricity - production">
            <text>234.5 billion kWh (2016 est.)</text>
         </section>
         <section name="Electricity - consumption">
            <text>207.1 billion kWh (2016 est.)</text>
         </section>
         <section name="Electricity - exports">
            <text>16.55 billion kWh (2016 est.)</text>
         </section>
         <section name="Electricity - imports">
            <text>10.56 billion kWh (2016 est.)</text>
         </section>
         <section name="Electricity - installed generating capacity">
            <text>50.02 million kW (2016 est.)</text>
         </section>
         <section name="Electricity - from fossil fuels">
            <text>85% of total installed capacity (2016 est.)</text>
         </section>
         <section name="Electricity - from nuclear fuels">
            <text>4% of total installed capacity (2017 est.)</text>
         </section>
         <section name="Electricity - from hydroelectric plants">
            <text>1% of total installed capacity (2017 est.)</text>
         </section>
         <section name="Electricity - from other renewable sources">
            <text>10% of total installed capacity (2017 est.)</text>
         </section>
         <section name="Crude oil - production">
            <text>1,600 bbl/day (2018 est.)</text>
         </section>
         <section name="Crude oil - exports">
            <text>0 bbl/day (2015 est.)</text>
         </section>
         <section name="Crude oil - imports">
            <text>404,000 bbl/day (2015 est.)</text>
         </section>
         <section name="Crude oil - proved reserves">
            <text>15 million bbl (1 January 2018 est.)</text>
         </section>
         <section name="Refined petroleum products - production">
            <text>487,100 bbl/day (2015 est.)</text>
         </section>
         <section name="Refined petroleum products - consumption">
            <text>621,000 bbl/day (2016 est.)</text>
         </section>
         <section name="Refined petroleum products - exports">
            <text>105,600 bbl/day (2015 est.)</text>
         </section>
         <section name="Refined petroleum products - imports">
            <text>195,200 bbl/day (2015 est.)</text>
         </section>
         <section name="Natural gas - production">
            <text>906.1 million cu m (2017 est.)</text>
         </section>
         <section name="Natural gas - consumption">
            <text>5.069 billion cu m (2017 est.)</text>
         </section>
         <section name="Natural gas - exports">
            <text>0 cu m (2017 est.)</text>
         </section>
         <section name="Natural gas - imports">
            <text>4.162 billion cu m (2017 est.)</text>
         </section>
         <section name="Natural gas - proved reserves">
            <text>0 cu m (1 January 2012 est.)</text>
         </section>
      </category>
      <category name="Communications">
         <section name="Telephones - fixed lines">
            <text name="total subscriptions">2,098,802 (2020)</text>
            <text name="subscriptions per 100 inhabitants">3.54 (2020 est.)</text>
         </section>
         <section name="Telephones - mobile cellular">
            <text name="total subscriptions">95,959,439 (2020)</text>
            <text name="subscriptions per 100 inhabitants">161.8 (2020 est.)</text>
         </section>
         <section name="Telecommunication systems">
            <text name="general assessment">one of the most advanced infrastructures on the continent; investment by operators and municipal providers to improve network capability focused on fiber and LTE to extend connectivity; increase in Internet use for e-commerce, e-government, and e-health; government funds to improve broadband to more municipalities; high mobile penetration rate and FttP to 90% of the premises; regulatory intervention has improved telecommunications market; 5G in Capetown with additional auction and tests; importer of broadcasting equipment and computers from China (2020)</text>
            <text name="domestic">fixed-line over 3 per 100 persons and mobile-cellular nearly 162 telephones per 100 persons; consists of carrier-equipped open-wire lines, coaxial cables, microwave radio relay links, fiber-optic cable, radiotelephone communication stations, and wireless local loops; key centers are Bloemfontein, Cape Town, Durban, Johannesburg, Port Elizabeth, and Pretoria (2020)</text>
            <text name="international">country code - 27; landing points for the WACS, ACE, SAFE, SAT-3, Equiano, SABR, SAEx1, SAEx2, IOX Cable System, METISS, EASSy, and SEACOM/ Tata TGN-Eurasia&amp;nbsp;fiber-optic submarine cable systems connecting South Africa, East Africa, West Africa, Europe,&amp;nbsp;Southeast Asia, Asia, South America, Indian Ocean Islands,&amp;nbsp;and the&amp;nbsp;US;&amp;nbsp;satellite earth stations - 3 Intelsat (1 Indian Ocean and 2 Atlantic Ocean) (2019)</text>
            <text name="note">the COVID-19 pandemic continues to have a significant impact on production and supply chains globally; since 2020, some aspects of the telecom sector have experienced downturn, particularly in mobile device production; many network operators delayed upgrades to infrastructure; progress towards 5G implementation was postponed or slowed in some countries; consumer spending on telecom services and devices was affected by large-scale job losses and the consequent restriction on disposable incomes; the crucial nature of telecom services as a tool for work and school from home became evident, and received some support from governments</text>
         </section>
         <section name="Broadcast media">
            <text>the South African Broadcasting Corporation (SABC) operates 4 TV stations, 3 are free-to-air and 1 is pay TV; e.tv, a private station, is accessible to more than half the population; multiple subscription TV services provide a mix of local and international channels; well-developed mix of public and private radio stations at the national, regional, and local levels; the SABC radio network, state-owned and controlled but nominally independent, operates 18 stations, one for each of the 11 official languages, 4 community stations, and 3 commercial stations; more than 100 community-based stations extend coverage to rural areas</text>
         </section>
         <section name="Internet country code">
            <text>.za</text>
         </section>
         <section name="Internet users">
            <text name="total">38.19 million (2021 est.)</text>
            <text name="percent of population">56.17% (2019 est.)</text>
         </section>
         <section name="Broadband - fixed subscriptions">
            <text name="total">1,303,057 (2020)</text>
            <text name="subscriptions per 100 inhabitants">2.2 (2020 est.)</text>
         </section>
      </category>
      <category name="Military and Security">
         <section name="Military and security forces">
            <text>South African National Defense Force (SANDF): South African Army (includes Reserve Force), South African Navy (SAN), South African Air Force (SAAF), South African Military Health Services; South African Police Service (includes Special Task Force for counterterrorism, counterinsurgency, and hostage rescue operations) (2021)</text>
         </section>
         <section name="Military expenditures">
            <text name="Military Expenditures 2021">0.8% of GDP (2021 est.)</text>
            <text name="Military Expenditures 2020">1.1% of GDP (2020)</text>
            <text name="Military Expenditures 2019">1% of GDP (2019)</text>
            <text name="Military Expenditures 2018">1% of GDP (2018)</text>
            <text name="Military Expenditures 2017">1% of GDP (2017)</text>
         </section>
         <section name="Military and security service personnel strengths">
            <text>the South African National Defence Force (SANDF) is comprised of approximately 75,000 personnel (40,000 Army; 7,000 Navy; 10,000 Air Force; 8,000 Military Health Service; 10,000 other, including administrative, logistics, military police); 180,000 South African Police Service (2021)</text>
         </section>
         <section name="Military equipment inventories and acquisitions">
            <text>the SANDF's inventory consists of a mix of domestically-produced and foreign-supplied equipment; South Africa's domestic defense industry produced most of the Army's major weapons systems (some were jointly-produced with foreign companies), while the Air Force and Navy inventories include a mix of European, Israeli, and US-origin weapons systems; since 2010, Sweden is the largest supplier of weapons to the SANDF (2021)</text>
         </section>
         <section name="Military service age and obligation">
            <text>18-26 years of age for voluntary military service; women are eligible to serve in noncombat roles; 2-year service obligation (2021)</text>
            <text name="note">note - in 2019, women comprised about 30% of the SANDF</text>
         </section>
         <section name="Military deployments">
            <text>950 Democratic Republic of the Congo (MONUSCO) (Oct 2021)</text>
            <text name="note">note - in 2021, South Africa sent a contingent of about 1,500 troops to Mozambique as part of a Southern African Development Community (SADC) force to help quell an insurgency</text>
         </section>
         <section name="Military - note">
            <text>the SANDF was created in 1994 to replace the South African Defense Force (SADF); the SANDF was opened to all South Africans who met military requirements, while the SADF was a mostly white force (only whites were subject to conscription) with non-whites only allowed to join in a voluntary capacity; the SANDF also absorbed members of the guerrilla and militia forces of the various anti-apartheid opposition groups, including the African National Congress, the Pan Africanist Congress, and the Inkatha Freedom Party, as well as the security forces of the formerly independent Bantustan homelands as of 2021, the SANDF was one of Africa’s most capable militaries; it participated regularly in African and UN peacekeeping missions and had the ability to independently deploy throughout Africa; over the past decade, however, its operational readiness and modernization programs have been hampered by funding shortfalls</text>
         </section>
      </category>
      <category name="Transnational Issues">
         <section name="Disputes - international">
            <text>South Africa has placed military units to assist police operations along the border of Lesotho, Zimbabwe, and Mozambique to control smuggling, poaching, and illegal migration; the governments of South Africa and Namibia have not signed or ratified the text of the 1994 Surveyor's General agreement placing the boundary in the middle of the Orange River</text>
         </section>
         <section name="Refugees and internally displaced persons">
            <text name="refugees  country of origin ">23,054 (Somalia), 15,629 (Ethiopia) (mid-year 2021); 57,595 (Democratic Republic of the Congo) (refugees and asylum seekers) (2022)</text>
            <text name="IDPs">5,000 (2020)</text>
         </section>
         <section name="Illicit drugs">
            <text>leading regional importer of chemicals used in the production of illicit drugs especially synthetic drugs</text>
         </section>
      </category>
   </categories>
</country>
