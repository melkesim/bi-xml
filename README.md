# BI-XML

## semestral work

Semestrální práci jsem vypracoval sám, pomocí tutoriálů a materiálů na **[ZVON](http://zvon.org/BI-XML/)**

Ke zpracování jsem si vybral státy: 
_**Netherlands, New Zealand, South Africa, Ukraine**_

Data jsem stáhl ve formě _json_ souborů, převedl pomocí online nástroje do xml a následně tranformoval pomocí xslt stylesheetu do finální podoby.

Výsledné soubory jsou ve složce data/xml-source. Zmíněný stylesheet je ve složce data/xml-raw.

Pomocí **script.sh** lze: 
- spojit hotové xml dokumenty do jednoho dokumentu
- zvalidovat výsledný dokument pomocí DTD i RelaxNG
- vytvořit HTML dokumenty
- vytvořit PDF dokument



