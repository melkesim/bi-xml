<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes" />

    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="my-page" page-height="29.7cm" page-width="21.0cm">
                    <fo:region-body margin="2cm"/>
                    <fo:region-after extent="2cm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="my-page">
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block text-align="center">
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block-container height="100%" text-align="center" display-align="center">
                        <fo:block font-size="10pt">Semestral work</fo:block>
                        <fo:block font-size="50pt" margin-top="0.5cm" margin-bottom="0.5cm">The World Factbook</fo:block>
                        <fo:block-container font-size="10pt" color="#f0ab00">
                            <fo:block >FIT CTU</fo:block>
                            <fo:block >BI-XML</fo:block>
                            <fo:block >2021-2022</fo:block>
                        </fo:block-container>
                        <fo:block margin-top="7cm" font-size="10pt" font-weight="bold">Šimon Melkes</fo:block>
                    </fo:block-container>
                    <fo:block margin-bottom="15pt" font-weight="bold" font-size="200%">Contents</fo:block>
                    <xsl:apply-templates mode="contents-country"/>
                    <fo:block page-break-before="always"/>
                    <xsl:apply-templates/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    
    <xsl:template match="country" mode="contents-country">
        <fo:block>
            <fo:block text-align-last="justify" font-weight="bold" font-size="120%" margin-top="5pt">
                <fo:basic-link internal-destination="{@name}">
                    <xsl:value-of select="@name"/>
                    <fo:leader leader-pattern="dots"/>
                    <fo:page-number-citation ref-id="{@name}"/>
                </fo:basic-link>
            </fo:block>
            <xsl:apply-templates mode="contents-category"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="country">
        <fo:block id="{@name}">
            <fo:block font-size="300%" font-weight="bold" margin-bottom="10pt" margin-left="-5pt" page-break-before="always">
                <xsl:value-of select="@name"/>
            </fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="images">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="image">
        <fo:inline>
            <fo:external-graphic src="{@src}" content-height="scale-to-fit" height="2.00in"  content-width="2.00in"/>
        </fo:inline>
    </xsl:template>

    <xsl:template match="categories">
        <fo:block margin-left="20pt">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="category" mode="contents-category">
        <fo:block margin-left="5pt" text-align-last="justify">
            <fo:basic-link internal-destination="{generate-id(.)}">
                <xsl:value-of select="@name"/>
                <fo:leader leader-pattern="dots"/>
                <fo:page-number-citation ref-id="{generate-id(.)}"/>
            </fo:basic-link>
        </fo:block>
    </xsl:template>

    <xsl:template match="category">
        <fo:block margin-top="40pt" id="{generate-id(.)}">
            <fo:block font-size="200%" font-weight="bold">
                <xsl:value-of select="@name"/>
            </fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="section">
        <fo:block margin-left="20pt" margin-top="30pt">
            <fo:block font-size="150%" margin-bottom="7pt">
                <xsl:value-of select="@name"/>
            </fo:block>
            <fo:block>
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="text[not(@name)]">
        <fo:block>
            <xsl:value-of select="."/>
        </fo:block>
    </xsl:template>

    <xsl:template match="text[@name]">
        <fo:block>
            <fo:inline font-weight="bold">
                <xsl:value-of select="@name"/>:
            </fo:inline>
            <xsl:value-of select="."/>
        </fo:block>
    </xsl:template>

</xsl:stylesheet>
