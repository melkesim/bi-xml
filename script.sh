#!/bin/bash

## script which:
# clears output directory
# merges all countries in one project xml file
# validates project file with dtd
# validates project file with relaxNG
# creates html file
# creates PDF file

echo "________________________________________________________________________________"
echo "Deleting output directory"
rm -rf ./output
mkdir output



echo "________________________________________________________________________________"
echo "Merging countries:"
echo "-----------------"
ls data/xml-source/*.xml | xargs -n 1 basename
echo "______________________________"
MERGED=output/merged.xml
xmllint --output $MERGED --dropdtd --noent data/merge.xml
echo "merged into: $MERGED"
echo "________________________________________________________________________________"

echo "Validating with DTD"
xmllint --noout --dtdvalid validation/validate.dtd $MERGED
echo "________________________________________________________________________________"
echo "Validating with relaxNG"
xmllint --noout --relaxng validation/validate.rng $MERGED


echo "________________________________________________________________________________"
echo "Creating html"
java -jar lib/saxon/saxon-he-10.3.jar output/merged.xml html/toHTML.xsl -o:output/index.html
echo "________________________________________________________________________________"
echo "Creating pdf"
fop -xml output/merged.xml -xsl pdf/toPDF.xsl -pdf output/all.pdf 2>/dev/null
echo "====ALL DONE===="












